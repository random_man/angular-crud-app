export interface Department  {
    department_id?: number;
    department_name: string;
    company_id: number;
}