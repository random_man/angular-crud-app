export interface User {
    user_id?: number;
    nickname: string;
    department_id: number;
    person_id: number;
    position_id: number;
    super_user: boolean;
}