import { Component, OnInit } from '@angular/core';
import {Person} from '../../models/Person';
import {PersonService} from '../../services/person-service';

@Component({
  selector: 'persons-component',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css']
})
export class PersonsComponent implements OnInit{ 
  title: 'Angular CRUD';
  persons: Person[];
  columns = [
    { prop: 'person_id', name: 'ID'},
    { prop: 'first_name', name: 'First Name' },
    { prop: 'last_name', name: 'Last Name' },
    { prop: 'middle_name', name: 'Middle Name'},
    { prop: 'email', name: 'Email'},
    { prop: 'phone_number', name: 'Phone Number'},
  ];

  constructor(private personService: PersonService) {}

  ngOnInit(): void {
    this.getPersons();
  }

  getPersons():void {
    this.personService.getAll()
      .then(persons => this.persons = persons);
  }
}