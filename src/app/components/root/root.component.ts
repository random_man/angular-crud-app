import { Component, OnInit } from '@angular/core';
import {NavbarComponent} from '../navbar/navbar.component';
import {PersonService} from '../../services/person-service';
import {DatabaseService} from '../../services/database.service';


import '@angular/material/prebuilt-themes/indigo-pink.css';
import '@swimlane/ngx-datatable/release/index.css'
import '@swimlane/ngx-datatable/release/themes/material.css';
import '../../../assets/css/styles.css';

@Component({
  selector: 'my-app',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css'],
  providers: [ PersonService]
})
export class RootComponent implements OnInit{ 
  title: 'Angular CRUD';

  constructor(private personService: PersonService){}

  ngOnInit(): void {
    this.initApp();
  }

  initApp(): void{
    this.personService.getAll()
      .then(persons => {
        if(persons.length === 0) {
          console.log(persons);
          this.personService.fillPersons();
        }
      });
  }
}
