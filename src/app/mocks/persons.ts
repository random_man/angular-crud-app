import {Person} from '../models/Person';

export let PERSONS: Person[] = [
    {
        person_id: 0,
        first_name: 'Bill',
        last_name: 'Geits',
        middle_name: 'Null',
        email: 'boss@microsoft.com',
        phone_number: '+131195982000'
    },
    {
        person_id: 1,
        first_name: 'Sergey',
        last_name: 'brin',
        middle_name: 'Null',
        email: 'boss@google.com',
        phone_number: '+131195982000'
    },
    {
        person_id: 2,
        first_name: 'Stiv',
        last_name: 'Jobs',
        middle_name: 'Null',
        email: 'boss@apple.com',
        phone_number: '+131195982000'
    },
]