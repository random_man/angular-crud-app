import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialImportModule} from './modules/material.import.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AppRoutingModule} from './modules/app.routing.module';
import {DatabaseService} from './services/database.service'; 
import { RootComponent } from './components/root/root.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {PersonsComponent} from './components/persons/persons.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialImportModule,
    AppRoutingModule,
    NgxDatatableModule,
  ],
  declarations: [
    RootComponent,
    NavbarComponent,
    PersonsComponent,
  ],
  providers:[
    DatabaseService,
  ],
  bootstrap: [ RootComponent ]
})
export class AppModule { }
