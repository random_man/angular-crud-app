import {Injectable} from '@angular/core';
import Dexie from "dexie";
import {Person} from "../models/Person";
import {User} from "../models/User";
import {Position} from "../models/Position";
import {Department} from "../models/Department";
import {Company} from "../models/Company";

@Injectable()
export class DatabaseService extends Dexie {
    persons: Dexie.Table<Person, number>;
    users: Dexie.Table<User, number>;
    positions: Dexie.Table<Position, number>;
    departments: Dexie.Table<Department, number>;
    companyes: Dexie.Table<Company, number>;

    constructor() {
        super('database');
        this.version(1).stores({
            persons: '++person_id, email',
            users: '++user_id',
            positions: '++position_id',
            departments:'++department_id',
            companyes:'++company_id',
        });
    }
}