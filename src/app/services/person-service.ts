import {Injectable} from '@angular/core';
import Dexie from 'dexie';
import 'rxjs/add/operator/toPromise';
import {DatabaseService} from './database.service';

import {Person} from '../models/Person';
import {PERSONS} from '../mocks/persons';

@Injectable()
export class PersonService{
    persons: Dexie.Table<Person, number>;

    constructor(private db: DatabaseService) {
        this.persons = this.db.table('persons');
    }

    getAll(){
        return this.persons.toArray();
    }

    fillPersons() {
        PERSONS.map((person) => {
            this.add(person);
        });
    }

    add(person: Person) {
        return this.persons.add(person);
    }

    update(id: number, person: Person) {
        return this.persons.update(id, person);
    }

    remove(id: number) {
        return this.persons.delete(id);
    }
}